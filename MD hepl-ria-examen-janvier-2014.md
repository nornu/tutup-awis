# HEPL - RIA : Examen de janvier 2014

L'examen de janvier portera sur la réalisation d'une application web mobile.

## Énoncé

L'application sera une réinterprétation personnelle de l'application [Cash-Cache](http://app.cash-cache.com/), qui utilise la géolocalisation du visiteur pour lui lister les distributeurs de billets les plus proches de lui.

Sur base de l'API mise à votre disposition, il vous faudra afficher au minimum 3 vues différentes : 

* Une liste des distributeurs les plus proches
* Une vue "carte" des distributeurs
* Une vue "détail" d'un distributeur sélectionné

Le tout devra bien sûr être consultable sur **mobile**, mais devra s'adapter aux autres médias, principalement **tablettes** et **desktop**.

Puisque le sujet principal de notre cours est javascript, j'attends de vous une utilisation quasi-exclusive de ce dernier. De plus, puisque c'est une application web, je m'attends au moins de rechargement de page possible. Le mot d'ordre est donc : ajax, *ajax*, **ajax**.

L'énoncé est très court afin de vous laisser un maximum de liberté. Si vous avez des idées, des fonctions à ajouter, faites-le, ça ne peut pas vous nuire.  
Mais attention : rajouter des trucs, c'est bien, mais assurez-vous que ça marche.

De plus, et j'insiste, j'attends de vous une application *jolie à l'oeil* et agréable à utiliser... et différente de l'application en place : ce serait trop facile :)

## Contraintes techniques

Il n'y a aucune contrainte technique spécifique, vous êtes libres d'utiliser tout ce que vous voulez, pour tous les aspects de votre travail.

En contrepartie, chacun de vos choix peut être soumis à une justification lorsque nous parcourerons ensemble votre application à l'examen.

## Modalités

Lors de l'examen, vous m'apporterez les sources complètes de vos applications. 

Je me connecterai ensuit à votre application web via, au choix : mon iPhone, mon iPad, une tablette Androïd et/ou mon macbook.  
Votre application doit être en ligne : même si je reprends vos sources, je veux que ce soit hébergé quelque part pour l'examen.

Nous profiterons de ce petit test rapide pour discuter ensemble de vos choix techniques, des difficultés rencontrées, etc...

* * *

## API: cash-cache

L'api de cash-cache à votre disposition est simplissime : il vous suffit de faire une requête en `GET` à l'adresse suivante : `http://ccapi.superacid.be/terminals`

Cette adresse peut recevoir 3 paramètres :

* **latitude:** la latitude du point de recherche
* **longitude:** la longitude du point de recherche
* **radius (optionnel):** le rayon (en km) de recherche (limité à 20km), si le paramètre radius est absent, l'API commence par un rayon d'1km et l'aggrandis jusqu'à trouver suffisament de résultats, toujours avec un maximum de 20km.

###### Exemple d'appel : 

[http://ccapi.superacid.be/terminals?latitude=50.633333&longitude=5.566667&radius=10](http://ccapi.superacid.be/terminals?latitude=50.633333&longitude=5.566667&radius=10)

L'API vous retourne donc un json avec toutes les informations dont vous avez besoin.