/* HEPL RIA 2013 - Examen
 *
 * JS Document - /js/script.js
 *
 * coded by Julien Cheron
 * started at 27-12-13
 */

/* jshint boss: true, curly: true, eqeqeq: true, eqnull: true, immed: true, latedef: true, newcap: true, noarg: true, browser: true, jquery: true, noempty: true, sub: true, undef: true, unused: true, white: false */

(function ($){
	var gMap,
		aMarkers = [],
		gMarker,
		oMyPosition = {},
		aCashBanksInfo,
		oBankId,
		gAnimation,
		gInfoWindow,
		divWaiting = $('#waiting'),
		directionsDisplay = new google.maps.DirectionsRenderer({
			suppressMarkers : true,
			draggable : true,
		}),
		directionsService = new google.maps.DirectionsService(),
		gMapStylers = [
			{
				elementType : "geometry.stroke",
				featureType : "all",
				stylers : [{weight : "1"}]
			},
			{
				elementType : "geometry.fill",
				featureType : "all",
				stylers : [{weight : "3"}]
			},
			{
				elementType : "labels.icon",
				featureType :  "all",
				stylers : [{visibility : "off"}]
			},
			{
				elementType : "geometry.fill",
				featureType : "landscape",
				stylers : [{color : "#05111F"}]
			},
			{
				elementType : "geometry.fill",
				featureType : "road",
				stylers : [{color:"#2e4a58"}]
			},
			{
				elementType : "geometry.fill",
				featureType : "water",
				stylers : [{color : "#adc5ce"}]
			},
			{
				elementType : "geometry.fill",
				featureType : "poi",
				stylers : [{color : "#dddddd"}]
			},
			{
				elementType : "labels.text.fill",
				featureType : "all",
				stylers : [{color : "#000000"}]
			}
		];

	var generateGoogleMap = function(oStylers){
		gMap = new google.maps.Map(document.getElementById("gmap"), {
			center : new google.maps.LatLng(50.638793, 5.577911), //Lg
			zoom : 15,
			scrollwheel : true,
			mapTypeId : google.maps.MapTypeId.PLAN,
			zoomControlOptions : {position : google.maps.ControlPosition.RIGHT_CENTER},
			mapTypeControl : false,
			streetViewControl : false,
			panControl : false,
			styles : oStylers
		});
	};

	var getPositionSucces = function(oPosition){
		divWaiting.hide().empty().append('Nous vous avons trouvé!').show().css({'animation-name': 'stop'});
		$('div.button').fadeIn();
		$('p#message').hide();
		oMyPosition = oPosition.coords;
		updateGoogleMapPosition(oMyPosition);
	};

	var getPositionError = function(oError){
		divWaiting.empty().css({'animation-name' : 'stop'});
		if(oError.code === 1){
			divWaiting.append('Vous avez refusé que nous accédions à votre position.<br>Voulez-vous l\'entrer manuellement?');
		}else if(oError.code === 2){
			divWaiting.append('Votre position est inaccessible.<br>Voulez-vous l\'entrer manuellement?');
		}else if(oError.code === 3){
			divWaiting.append('Nous n\'avons pas trouvé votre position.<br>Voulez-vous l\'entrer manuellement?');
		}
		divWaiting.append('<form action="/"><input class="input" type="text" /><input type="submit" value="Envoyer" id="newPosition" href="javascript:void(0)"></form><div id="results"></div>');
		$('#newPosition').on('click',setNewPosition);
	};

	var setNewPosition = function(e){
		e.preventDefault();
		var sInputValue = $('.input').val(),
			oTempResults;
		$.ajax({
			url : 'https://maps.googleapis.com/maps/api/geocode/json?address=' + sInputValue + '&sensor=true',
			type : 'GET',
			async : false,
			success:function(oResponse){
				if(oResponse.status === "OK"){
					oTempResults = oResponse.results[0];
				}
			},
			error:function(oError){
				$('#results').empty().append(oError.responseText);
			}
		});
		$('#results').empty().append('<p>Nous avons trouvé l\'adresse suivante : <a id="newLocation" class="button-small" href="javascript:void(0)"><small>' + oTempResults.formatted_address + '</small></a></p>');
		oMyPosition.latitude = oTempResults.geometry.location.lat;
		oMyPosition.longitude = oTempResults.geometry.location.lng;
		updateGoogleMapPosition();
		$('#newLocation').on('click',requestData);
	};

	var updateGoogleMapPosition = function(){
		var gMyPosition = new google.maps.LatLng(oMyPosition.latitude, oMyPosition.longitude);
		gMap.panTo(gMyPosition);
	};

	var requestData = function(e){
		e.preventDefault();
		var iMyLat = oMyPosition.latitude,
			iMyLng = oMyPosition.longitude;

		$.get('http://ccapi.superacid.be/terminals?latitude=' + iMyLat + '&longitude=' + iMyLng + '&radius=5',
			function(oResponse){
				if(!oResponse.error){
					if(oResponse.data.length === 0){
						divWaiting.empty().append('Nous n\'avons trouvé aucun résultats.');
					}else{
						aCashBanksInfo = oResponse.data;
						showBanks(aCashBanksInfo);
					}
				}else{
					divWaiting.empty().append('Une erreur s\'est produite. Contactez l\'administeur avec le message suivant : "' + oResponse.error + '"');
				}
			}).error(function(){
				divWaiting.empty().append('Une erreur de récupération des données s\'est produite. Rechargez la page.');
			});
	};

	var showBanks = function(aCashBanksInfo){
		var gLatLngBounds = new google.maps.LatLngBounds(),
			gMyPosition = new google.maps.LatLng(oMyPosition.latitude, oMyPosition.longitude),
			aMarkers = [];

		$('#wrapper').fadeOut();
		$('.page-header').fadeIn();
		$('#gmap').fadeOut().empty();

		generateGoogleMap();
		updateGoogleMapPosition();
		addMarker(gMyPosition, gMap, 'css/img/ME.png');

		for (var i = 0; i < aCashBanksInfo.length; i++) {
			if(aCashBanksInfo[i].bank !== null){
				var gCashBankPosition = new google.maps.LatLng(aCashBanksInfo[i].latitude, aCashBanksInfo[i].longitude),
				sIcon = 'css/img/ATM.png';
				aMarkers = addMarker(gCashBankPosition, gMap, sIcon, aCashBanksInfo[i]);
			}
		}

		for (var k = 0; k < 10; k++) {
			if(aMarkers[k]){
				gLatLngBounds.extend(aMarkers[k].position);
			}
		}
		gMap.fitBounds(gLatLngBounds);

		gInfoWindow = new google.maps.InfoWindow({content : "Chargement..."});

		for (var j = 0; j < aMarkers.length; j++) {
			var marker = aMarkers[j];
			google.maps.event.addListener(marker, 'click', setInfoWindowContent);
		}

		setMenu();
		setNearestBanks();
		$('#gmap').fadeIn();
		$('#overlay').detach();
	};

	var setInfoWindowContent = function(){
		var oInfoCashBank = this.infos;
		getInfoWindowContent(oInfoCashBank);
		gInfoWindow.open(gMap, this);
		triggerDirections(oMyPosition, this);
	};

	var addMarker = function(oPosition, gMap, sIcon, oInfoCashBank){
		var sBankID,
			iID,
			sTitle;
		if(!oInfoCashBank){
			gAnimation = google.maps.Animation.BOUNCE;
			sBankID = null;
			iID = null;
			sTitle = 'Ma Position';
		}else{
			gAnimation = google.maps.Animation.DROP;
			sBankID = oInfoCashBank.bank.id;
			iID = oInfoCashBank.id;
			sTitle = oInfoCashBank.bank.name;
		}
		gMarker = new google.maps.Marker({
			position : oPosition,
			map : gMap,
			title : sTitle,
			icon : sIcon,
			id : iID,
			animation : gAnimation,
			infos : oInfoCashBank,
			bankID : sBankID
		});
		aMarkers.push(gMarker);
		return aMarkers;
	};

	var getInfoWindowContent = function(oInfoCashBank){
		var oPostInfos = {};
		gInfoWindow.setContent(null);
		if(oInfoCashBank){
			oPostInfos.bank = {
				'address' : oInfoCashBank.address,
				'distance' : oInfoCashBank.distance,
				'icon' : oInfoCashBank.bank.icon,
				'name' : oInfoCashBank.bank.name,
				'url' : oInfoCashBank.bank.url,
				'color' : oInfoCashBank.bank.color,
				'id' : oInfoCashBank.id,
			};
		}else{
			oPostInfos = reverseGeocoding(oMyPosition);
		}
		$.ajax({
			url : './js/data/infowindow.php',
			type : 'POST',
			data : oPostInfos,
			success : function(oResponse){
				gInfoWindow.setContent(oResponse);
			},
			error: function(oError){
				gInfoWindow.setContent('<div id="infowindow">Contactez l\'administrateur du site avec le message suivant :<br>' + oError.responseText + '</div>');
			}
		});
	};

	var triggerDirections = function(oPosition, oMarker){
		var gPosition = new google.maps.LatLng(oPosition.latitude,oPosition.longitude);
		google.maps.event.addListenerOnce(gInfoWindow, 'content_changed', function(){
			$('#directions').on('click',function(e){
				e.preventDefault();
				directionsDisplay.setMap(null);
				generateDirection(gPosition, oMarker.position);
			});
		});
	};

	var generateDirection = function(origin, destination){
		var request = {
				origin : origin,
				destination : destination,
				travelMode : google.maps.TravelMode.WALKING
			};
		directionsService.route(request,
			function(result, status){
				if(status === google.maps.DirectionsStatus.OK){
					directionsDisplay.setMap(gMap);
					directionsDisplay.setDirections(result);
				}
		});

	};

	var setMenu = function(){
		var aInfoBanks = [];
		$('.submenu').hide();
		for (var i = 0; i < aCashBanksInfo.length; i++) {
			if(aCashBanksInfo[i].bank !== null){
				oBankId = aCashBanksInfo[i].bank.id;
				aInfoBanks[oBankId] = aCashBanksInfo[i].bank;
			}
		}

		for (var j = 0; j < aInfoBanks.length; j++) {
			if(aInfoBanks[j]){
				$('.submenu .cat_title').after('<li class="cat"><a data-bankId="' + aInfoBanks[j].id + '" style="color:#' + aInfoBanks[j].color + '" class="link_list" href="javascript:void(0)"><img src="' + aInfoBanks[j].icon + '"> ' + aInfoBanks[j].name + '</a></li>');
			}
		}

		$('.main-menu').css({'cursor':'pointer'}).on('click',function(){
			$('.submenu').slideToggle();
		});

		$('#allBanks').on('click',function(){
			for (var i = 0; i < aMarkers.length; i++) {
				aMarkers[i].setMap(gMap);
			}
		});

		$('.link_list').on('click',selectOneBank);
	};

	var selectOneBank = function(e){
		var dataset = e.currentTarget.dataset,
			iSelectedBankId,
			iBankId,
			iTemp = 0,
			gMyPosition = new google.maps.LatLng(oMyPosition.latitude, oMyPosition.longitude),
			gLatLngBounds = new google.maps.LatLngBounds();
		if(dataset.bankid){
			iSelectedBankId = parseInt(dataset.bankid);
		}else if(dataset.cashid){
			iSelectedBankId = parseInt(dataset.cashid);
		}
		for (var i = 1; i < aMarkers.length; i++) {
			aMarkers[i].setMap(gMap);
			if(dataset.bankid){
				iBankId = aMarkers[i].bankID;
			}else if(dataset.cashid){
				iBankId = aMarkers[i].id;
			}
			if(iSelectedBankId !== iBankId && iBankId !== null){
				aMarkers[i].setMap(null);
				directionsDisplay.setMap(null);
			}else{
				if(dataset.cashid){
					google.maps.event.trigger(aMarkers[i], 'click');
					generateDirection(gMyPosition,aMarkers[i].position);
				}else if(dataset.bankid){
					gLatLngBounds.extend(gMyPosition);
					if(iTemp < 5){
						gLatLngBounds.extend(aMarkers[i].position);
					}
					iTemp++;
					
				}
			}
		}
		gMap.fitBounds(gLatLngBounds);
	};

	var setNearestBanks = function(){
		for (var i = 0; i < 5; i++) {
			if(aCashBanksInfo[i]){
				var oBank = aCashBanksInfo[i].bank;
				$('footer .center').append('<div class="bank_container"><a data-cashId="' + aCashBanksInfo[i].id + '" href="javascript:void(0)" title="Voir l\'itinéraire vers cette banque" style="color:#' + oBank.color + '" class="bank"><img src="' + oBank.icon+ '" alt="Logo de la banque" />&nbsp;' + oBank.name + '<small>&plusmn;' + aCashBanksInfo[i].distance + 'm</small></a></div>');
			}
		}
		$('.bank').on('click',selectOneBank);
		$('footer').fadeIn();
	};

	var reverseGeocoding = function(oPosition){
		var oTempData = {};
		$.ajax({
			url : 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' + oPosition.latitude + ',' + oPosition.longitude + '&sensor=true',
			action : 'GET',
			async : false,
			success : function(oResponse){
				if(oResponse.status === 'OK'){
					oTempData.position = oResponse.results[0].formatted_address;
				}else{
					oTempData.position = 'Contactez l\'administrateur avec le message suivant :<br>' + oResponse.error_message;
				}
			},
			error:function(oError){
				oTempData.position = oError.responseText;
			}
		});
		return oTempData;
	};
	
	$(function(){
		$('.page-header').hide();
		$('footer').hide();

		generateGoogleMap(gMapStylers);

		$('div.button').hide().on('click',requestData);

		$('#overlay').css('pointer-events', 'none');

		if(navigator.geolocation){
			navigator.geolocation.getCurrentPosition(getPositionSucces,getPositionError, {enableHighAccuracy : false, timeout : 60000});
		}else{
			divWaiting.empty().append('La gélocalisation ne semble pas être activée sur votre navigateur.');
		}

	});

}).call(this,jQuery);