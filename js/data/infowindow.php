<?php 
if(isset($_POST['bank'])):
    $infoBank = $_POST['bank'];
?>
<div id="infowindow">
    <h1 id="firstHeading" class="firstHeading" style="color: #<?php echo($infoBank['color']) ?> "><img src="<?php echo($infoBank['icon']) ?>">&nbsp;<?php echo($infoBank['name']) ?></h1>
    <div id="bodyContent">
        <p id="address"><?php if(strlen($infoBank['address']) === 0){echo('Pas d\'adresse disponible');}else{echo($infoBank['address']);} ?></p>
        <p id="distance"><small>&plusmn; <?php echo($infoBank['distance']) ?>m vous sépare de cette banque</small></p>
        <div class="button-container">
            <a id="directions" class="button-small" href="javascript:void(0)" title="Montrer l'itinéraire pour aller à <?php echo($infoBank['address']); ?>">Montrer l'itinéraire</a>
        </div>
    </div>
</div>
<?php elseif (isset($_POST['position'])):?>
<div id="infowindow">
    <h1 id="firstHeading" class="firstHeading">Ma Position</h1>
    <div id="bodyContent">
        <p id="address"><?php echo($_POST['position']) ?></p>
    </div>
</div>
<?php else: ?>   
    <p>Nous n'avons pas d'informations sur cette banque</p>
    <p><a class="button-small" href="javascript:void(0)">Ajouter des informations</a></p>
<?php endif; ?>